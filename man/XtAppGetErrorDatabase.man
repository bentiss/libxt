.\" Copyright 1993 X Consortium
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, sublicense, and/or sell copies of the Software, and to
.\" permit persons to whom the Software is furnished to do so, subject to
.\" the following conditions:
.\"
.\" The above copyright notice and this permission notice shall be
.\" included in all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
.\" IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
.\" OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
.\" ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
.\" OTHER DEALINGS IN THE SOFTWARE.
.\"
.\" Except as contained in this notice, the name of the X Consortium shall
.\" not be used in advertising or otherwise to promote the sale, use or
.\" other dealings in this Software without prior written authorization
.\" from the X Consortium.
.\"
.ds tk X Toolkit
.ds xT X Toolkit Intrinsics \- C Language Interface
.ds xI Intrinsics
.ds xW X Toolkit Athena Widgets \- C Language Interface
.ds xL Xlib \- C Language X Interface
.ds xC Inter-Client Communication Conventions Manual
.ds Rn 3
.ds Vn 2.2
.hw XtApp-Get-Error-Database XtApp-Get-Error-Database-Text wid-get
.na
.de Ds
.nf
.in +0.4i
.ft CW
..
.de De
.ce 0
.fi
..
.de IN		\" send an index entry to the stderr
..
.de Pn
.ie t \\$1\fB\^\\$2\^\fR\\$3
.el \\$1\fI\^\\$2\^\fP\\$3
..
.de ZN
.ie t \fB\^\\$1\^\fR\\$2
.el \fI\^\\$1\^\fP\\$2
..
.de ny
..
.ny 0
.TH XtAppGetErrorDatabase __libmansuffix__ __xorgversion__ "XT FUNCTIONS"
.SH NAME
XtAppGetErrorDatabase, XtAppGetErrorDatabaseText \- obtain error database
.SH SYNTAX
.HP
XrmDatabase *XtAppGetErrorDatabase(\^XtAppContext \fIapp_context\fP);
.HP
void XtAppGetErrorDatabaseText(XtAppContext \fIapp_context\fP, char
*\fIname\fP, char *\fItype\fP, char *\fIclass\fP, char *\fIdefault\fP, char
*\fIbuffer_return\fP, int \fInbytes\fP, XrmDatabase \fIdatabase\fP);
.SH ARGUMENTS
.IP \fIapp_context\fP 1i
Specifies the application context.
.IP \fIbuffer_return\fP 1i
Specifies the buffer into which the error message is to be returned.
.ds Cl \ of the error message
.IP \fIclass\fP 1i
Specifies the resource class\*(Cl.
.IP \fIdatabase\fP 1i
Specifies the name of the alternative database that is to be used
or NULL if the application's database is to be used.
.IP \fIdefault\fP 1i
Specifies the default message to use.
.IP \fIname\fP 1i
.br
.ns
.IP \fItype\fP 1i
Specifies the name and type that are concatenated to form the resource name
of the error message.
.IP \fInbytes\fP 1i
Specifies the size of the buffer in bytes.
.SH DESCRIPTION
The
.BR XtAppGetErrorDatabase
function returns the address of the error database.
The \*(xI do a lazy binding of the error database and do not merge in the
database file until the first call to
.BR XtAppGetErrorDatbaseText .
.LP
The
.BR XtAppGetErrorDatabaseText
returns the appropriate message from the error database
or returns the specified default message if one is not found in the
error database.
.SH "SEE ALSO"
XtAppError(__libmansuffix__),
XtAppErrorMsg(__libmansuffix__)
.br
\fI\*(xT\fP
.br
\fI\*(xL\fP
