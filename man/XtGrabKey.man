.\" Copyright (c) 1993, 1994  X Consortium
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining a
.\" copy of this software and associated documentation files (the "Software"),
.\" to deal in the Software without restriction, including without limitation
.\" the rights to use, copy, modify, merge, publish, distribute, sublicense,
.\" and/or sell copies of the Software, and to permit persons to whom the
.\" Software furnished to do so, subject to the following conditions:
.\"
.\" The above copyright notice and this permission notice shall be included in
.\" all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.\" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.\" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
.\" THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
.\" WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
.\" OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.\" SOFTWARE.
.\"
.\" Except as contained in this notice, the name of the X Consortium shall not
.\" be used in advertising or otherwise to promote the sale, use or other
.\" dealing in this Software without prior written authorization from the
.\" X Consortium.
.\"
.ds tk X Toolkit
.ds xT X Toolkit Intrinsics \- C Language Interface
.ds xI Intrinsics
.ds xW X Toolkit Athena Widgets \- C Language Interface
.ds xL Xlib \- C Language X Interface
.ds xC Inter-Client Communication Conventions Manual
.ds Rn 3
.ds Vn 2.2
.hw XtGrab-Key XtUngrab-Key XtGrab-Keyboard XtUngrab-Keyboard
.hw XtGrab-Button XtUngrab-Button XtGrab-Pointer XtUngrab-Pointer wid-get
.na
.de Ds
.nf
.in +0.4i
.ft CW
..
.de De
.ce 0
.fi
..
.de IN		\" send an index entry to the stderr
..
.de Pn
.ie t \\$1\fB\^\\$2\^\fR\\$3
.el \\$1\fI\^\\$2\^\fP\\$3
..
.de ZN
.ie t \fB\^\\$1\^\fR\\$2
.el \fI\^\\$1\^\fP\\$2
..
.de ny
..
.ny 0
.TH XtGrabKey __libmansuffix__ __xorgversion__ "XT FUNCTIONS"
.SH NAME
XtGrabKey, XtUngrabKey, XtGrabKeyboard, XtUngrabKeyboard, XtGrabButton, XtUngrabButton, XtGrabPointer, XtUngrabPointer \- manage grabs
.SH SYNTAX
.HP
void XtGrabKey(Widget \fIwidget\fP, KeyCode \fIkeycode\fP, Modifiers
\fImodifiers\fP, Boolean \fIowner_events\fP, int \fIpointer_mode\fP, int
\fIkeyboard_mode\fP);
.HP
void XtUngrabKey(Widget \fIwidget\fP, KeyCode \fIkeycode\fP, Modifiers
\fImodifiers\fP);
.HP
int XtGrabKeyboard(Widget \fIwidget\fP, Boolean \fIowner_events\fP, int
\fIpointer_mode\fP, int \fIkeyboard_mode\fP, Time \fItime\fP);
.HP
void XtUngrabKeyboard(Widget \fIwidget\fP, Time \fItime\fP);
.HP
void XtGrabButton(Widget \fIwidget\fP, int \fIbutton\fP, Modifiers
\fImodifiers\fP, Boolean \fIowner_events\fP, unsigned int \fIevent_mask\fP,
int \fIpointer_mode\fP, int \fIkeyboard_mode\fP, Window \fIconfine_to\fP,
Cursor \fIcursor\fP);
.HP
void XtUngrabButton(Widget \fIwidget\fP, KeyCode \fIbutton\fP, Modifiers
\fImodifiers\fP);
.HP
int XtGrabPointer(Widget \fIwidget\fP, Boolean \fIowner_events\fP, unsigned
int \fIevent_mask\fP, int \fIpointer_mode\fP, int \fIkeyboard_mode\fP, Window
\fIconfine_to\fP, Cursor \fIcursor\fP, Time \fItime\fP);
.HP
void XtUngrabPointer(Widget \fIwidget\fP, Time \fItime\fP);
.SH ARGUMENTS
.IP \fIwidget\fP 1i
Specifies the widget in whose window the grab will occur. Must be of
class Core or a subclass thereof.
.sp 6p
.IP \fIkeycode\fP
.br
.ns
.IP \fImodifiers\fP
.br
.ns
.IP \fIowner_events\fP
.br
.ns
.IP \fIpointer_mode\fP
.br
.ns
.IP \fIkeyboard_mode\fP
.br
.ns
.IP \fItime\fP
.br
.ns
.IP \fIbutton\fP
.br
.ns
.IP \fIconfine_to\fP
.br
.ns
.IP \fIcursor\fP 1i
Specifies arguments to the associated Xlib function call.
.SH DESCRIPTION
.BR XtGrabKey
calls
.BR XGrabKey
specifying the widget's window as the grab window if the widget is
realized. The remaining arguments are exactly as for
.BR XGrabKey .
If the widget is not realized, or is later unrealized, the call to
.BR XGrabKey
will be performed (again) when the widget is realized and its window
becomes mapped. In the future, if
.BR XtDispatchEvent
is called with a
.BR KeyPress
event matching the specified keycode and modifiers (which may be
.BR AnyKey
or
.BR AnyModifier ,
respectively) for the widget's window, the Intrinsics will call
.BR XtUngrabKeyboard
with the timestamp from the
.BR KeyPress
event if either of the following conditions is true:
.IP \(bu 3
There is a modal cascade and the widget is not in the active subset
of the cascade and the keyboard was not previously grabbed, or
.IP \(bu 3
.BR XFilterEvent
returns
.BR True .
.LP
.BR XtUngrabKey
calls
.BR XUngrabKey
specifying the widget's window as the ungrab window if the widget is
realized. The remaining arguments are exactly as for
.BR XUngrabKey .
If the widget is not realized,
.BR XtUngrabKey
removes a deferred
.BR XtGrabKey
request, if any, for the specified widget, keycode, and modifiers.
.LP
If the specified widget is realized
.BR XtGrabKeyboard
calls
.BR XGrabKeyboard
specifying the widget's window as the grab window. The remaining
arguments and return value are exactly as for
.BR XGrabKeyboard .
If the widget is not realized,
.BR XtGrabKeyboard
immediately returns
.BR GrabNotViewable .
No future ungrab is implied by
.BR XtGrabKeyboard .
.LP
.BR XtUngrabKeyboard
calls
.BR XUngrabKeyboard
with the specified time.
.LP
.BR XtGrabButton
calls
.BR XGrabButton
specifying the widget's window as the grab window if the widget is
realized. The remaining arguments are exactly as for
.BR XGrabButton .
If the widget is not realized, or is later unrealized, the call to
.BR XGrabButton
will be performed (again) when the widget is realized and its window
becomes mapped. In the future, if
.BR XtDispatchEvent
is called with a
.BR ButtonPress
event matching the specified button and modifiers (which may be
.BR AnyButton
or
.BR AnyModifier ,
respectively) for the widget's window, the Intrinsics will call
.BR XtUngrabPointer
with the timestamp from the
.BR ButtonPress
event if either of the following conditions is true:
.IP \(bu 3
There is a modal cascade and the widget is not in the active subset
of the cascade and the pointer was not previously grabbed, or
.IP \(bu 3
.BR XFilterEvent
returns
.BR True .
.LP
.BR XtUngrabButton
calls
.BR XUngrabButton
specifying the widget's window as the ungrab window if the widget is
realized. The remaining arguments are exactly as for
.BR XUngrabButton .
If the widget is not realized,
.BR XtUngrabButton
removes a deferred
.BR XtGrabButton
request, if any, for the specified widget, button, and modifiers.
.LP
.BR XtGrabPointer
calls
.BR XGrabPointer
specifying the widget's window as the grab window. The remaining
arguments and return value are exactly as for
.BR XGrabPointer .
If the widget is not realized,
.BR XtGrabPointer
immediately returns
.BR GrabNotViewable .
No future ungrab is implied by
.BR XtGrabPointer .
.LP
.BR XtUngrabPointer
calls
.BR XUngrabPointer
with the specified time.
.SH "SEE ALSO"
.br
\fI\*(xT\fP
.br
\fI\*(xL\fP
