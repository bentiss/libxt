.\" Copyright 1993 X Consortium
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, sublicense, and/or sell copies of the Software, and to
.\" permit persons to whom the Software is furnished to do so, subject to
.\" the following conditions:
.\"
.\" The above copyright notice and this permission notice shall be
.\" included in all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
.\" IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
.\" OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
.\" ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
.\" OTHER DEALINGS IN THE SOFTWARE.
.\"
.\" Except as contained in this notice, the name of the X Consortium shall
.\" not be used in advertising or otherwise to promote the sale, use or
.\" other dealings in this Software without prior written authorization
.\" from the X Consortium.
.\"
.ds tk X Toolkit
.ds xT X Toolkit Intrinsics \- C Language Interface
.ds xI Intrinsics
.ds xW X Toolkit Athena Widgets \- C Language Interface
.ds xL Xlib \- C Language X Interface
.ds xC Inter-Client Communication Conventions Manual
.ds Rn 3
.ds Vn 2.2
.hw XtSet-Keyboard-Focus wid-get
.na
.de Ds
.nf
.in +0.4i
.ft CW
..
.de De
.ce 0
.fi
..
.de IN		\" send an index entry to the stderr
..
.de Pn
.ie t \\$1\fB\^\\$2\^\fR\\$3
.el \\$1\fI\^\\$2\^\fP\\$3
..
.de ZN
.ie t \fB\^\\$1\^\fR\\$2
.el \fI\^\\$1\^\fP\\$2
..
.de ny
..
.ny 0
.TH XtSetKeyboardFocus __libmansuffix__ __xorgversion__ "XT FUNCTIONS"
.SH NAME
XtSetKeyboardFocus \- focus events on a child widget
.SH SYNTAX
.HP
XtSetKeyboardFocus(Widget \fIsubtree\fP, Widget \fIdescendant\fP);
.SH ARGUMENTS
.IP \fIsubtree\fP 1i
Specifies either the widget in the subtree structure which is to receive the
keyboard event, or
.BR None .
Note that it is not an error to specify
.BR None
when no input focus was previously set.
.ds Wi for which the keyboard focus is to be set
.IP \fIdescendant\fP 1i
Specifies the widget \*(Wi.
.SH DESCRIPTION
If a future
.BR KeyPress
or
.BR KeyRelease
event occurs within the specified subtree,
.BR XtSetKeyboardFocus
causes
.BR XtDispatchEvent
to remap and send the event to the specified descendant widget.
.LP
When there is no modal cascade,
keyboard events can occur within a widget W in one of three ways:
.IP \(bu 5
W has the X input focus.
.IP \(bu 5
W has the keyboard focus of one of its ancestors,
and the event occurs within the ancestor or one of the ancestor's descendants.
.IP \(bu 5
No ancestor of W has a descendant within the keyboard focus,
and the pointer is within W.
.LP
When there is a modal cascade,
a widget W receives keyboard events if an ancestor of W is in the active
subset of the modal cascade and one or more of the previous conditions is
.BR True .
.LP
When subtree or one of its descendants acquires the X input focus
or the pointer moves into the subtree such that keyboard events would
now be delivered to subtree, a
.BR FocusIn
event is generated for the descendant if
.BR FocusNotify
events have been selected by the descendant.
Similarly, when W loses the X input focus
or the keyboard focus for one of its ancestors, a
.BR FocusOut
event is generated for descendant if
.BR FocusNotify
events have been selected by the descendant.
.SH "SEE ALSO"
XtCallAcceptFocus(__libmansuffix__)
.br
\fI\*(xT\fP
.br
\fI\*(xL\fP
