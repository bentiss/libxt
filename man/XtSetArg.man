.\" Copyright 1993 X Consortium
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, sublicense, and/or sell copies of the Software, and to
.\" permit persons to whom the Software is furnished to do so, subject to
.\" the following conditions:
.\"
.\" The above copyright notice and this permission notice shall be
.\" included in all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
.\" IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
.\" OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
.\" ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
.\" OTHER DEALINGS IN THE SOFTWARE.
.\"
.\" Except as contained in this notice, the name of the X Consortium shall
.\" not be used in advertising or otherwise to promote the sale, use or
.\" other dealings in this Software without prior written authorization
.\" from the X Consortium.
.\"
.ds tk X Toolkit
.ds xT X Toolkit Intrinsics \- C Language Interface
.ds xI Intrinsics
.ds xW X Toolkit Athena Widgets \- C Language Interface
.ds xL Xlib \- C Language X Interface
.ds xC Inter-Client Communication Conventions Manual
.ds Rn 3
.ds Vn 2.2
.hw XtSet-Arg XtMerge-Arg-Lists wid-get
.na
.de Ds
.nf
.in +0.4i
.ft CW
..
.de De
.ce 0
.fi
..
.de IN		\" send an index entry to the stderr
..
.de Pn
.ie t \\$1\fB\^\\$2\^\fR\\$3
.el \\$1\fI\^\\$2\^\fP\\$3
..
.de ZN
.ie t \fB\^\\$1\^\fR\\$2
.el \fI\^\\$1\^\fP\\$2
..
.de ny
..
.ny 0
.TH XtSetArg __libmansuffix__ __xorgversion__ "XT FUNCTIONS"
.SH NAME
XtSetArg, XtMergeArgLists \- set and merge ArgLists
.SH SYNTAX
.HP
int XtSetArg(Arg \fIarg\fP, String \fIname\fP, XtArgVal \fIvalue\fP);
.HP
ArgList XtMergeArgLists(ArgList \fIargs1\fP, Cardinal \fInum_args1\fP, ArgList
\fIargs2\fP, Cardinal \fInum_args2\fP);
.SH ARGUMENTS
.IP \fIarg\fP 1i
Specifies the name-value pair to set.
.IP \fIargs1\fP 1i
Specifies the first
.BR ArgList .
.IP \fIargs2\fP 1i
Specifies the second
.BR ArgList .
.IP \fInum_args1\fP 1i
Specifies the number of arguments in the first argument list.
.IP \fInum_args2\fP 1i
Specifies the number of arguments in the second argument list.
.IP \fIname\fP 1i
Specifies the name of the resource.
.IP \fIvalue\fP 1i
Specifies the value of the resource if it will fit in an
.BR XtArgVal
or the address.
.SH DESCRIPTION
The
.BR XtSetArg
function is usually used in a highly stylized manner to
minimize the probability of making a mistake; for example:
.LP
.RS
.nf
Arg args[20];
int n;

n = 0;
XtSetArg(args[n], XtNheight, 100);      n++;
XtSetArg(args[n], XtNwidth, 200);       n++;
XtSetValues(widget, args, n);
.fi
.RE
.LP
Alternatively, an application can statically declare the argument list
and use
.BR XtNumber :
.LP
.RS
.nf
static Args args[] = {
        {XtNheight, (XtArgVal) 100},
        {XtNwidth, (XtArgVal) 200},
};
XtSetValues(Widget, args, XtNumber(args));
.fi
.RE
.LP
Note that you should not use auto-increment or auto-decrement
within the first argument to
.BR XtSetArg .
.BR XtSetArg
can be implemented as a macro that dereferences the first argument twice.
.LP
The
.BR XtMergeArgLists
function allocates enough storage to hold the combined
.BR ArgList
structures and copies them into it.
Note that it does not check for duplicate entries.
When it is no longer needed,
free the returned storage by using
.BR XtFree .
.SH "SEE ALSO"
XtOffset(__libmansuffix__)
.br
\fI\*(xT\fP
.br
\fI\*(xL\fP
